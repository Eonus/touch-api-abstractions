import { paramTypes } from './index'
export const getParam = (obj:any, name:string, type:paramTypes, canBeUndefined?:boolean) => {
    try {
        if ((!obj)||(obj[name] == undefined)||(obj[name] == null))
            throw Error("No " + name + '(' + type + ') sended')
    }
    catch (e) {
        if (canBeUndefined)
            return undefined

        else throw e
    }
    if (type == paramTypes.any)
        return obj[name]
    if ((typeof obj[name] != type))
        throw Error("False " + name + ' param type - ' + typeof obj[name] + ' instead ' + type)
    return obj[name]
}
