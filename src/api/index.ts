import { Template } from "./template";

export enum paramTypes {
    boolean = "boolean",
    string = "string",
    object = "object",
    any = "any",
    number = "number"
}

export abstract class ApiAbstract <App, Request, Response> {
    protected checkTokenAccess:(token:string) => Promise<boolean>
    protected template:Template
    protected app:App

    constructor (template:Template, app:App, checkTokenAccess:(token:string) => Promise<boolean>) {
        this.template = template
        this.app = app
        this.checkTokenAccess = checkTokenAccess
    }
    protected checkExist = (endpoint:string) => {
        if (!this.template[endpoint])
            throw Error('Such endpoint was not fount')
    }
    protected abstract getBody (req:Request) : any
    protected abstract checkAccess (endpoint:string, info:any) : Promise <void>
    protected abstract checkParamsAndCall (endpoint:string, info:any) : Promise <any>
    protected abstract sendError (error:Error, id:string, res:Response) : Promise <void>
    protected abstract sendSuccess (res:Response, id:string, info:any) : Promise <void>
    protected abstract call (endpoint:any, req:Request, res:Response) : Promise <void>
}