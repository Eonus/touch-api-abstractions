import { Template } from "./template";

export interface Templatable {
    getTemplate():Template
}