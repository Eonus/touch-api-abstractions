import { paramTypes } from './index'

export interface Template {
    [key:string]:{
        function:Function,
        params:Array <{
            type:paramTypes, 
            name:string,
            canBeUndefined?:boolean
        }>,
        needsLogin?:boolean,
        needsStartInfo?:boolean
    }
}