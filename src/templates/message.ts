export interface BaseMessage {
    from:string
    to:string
    text:string | null
    content:{
        type:string,
        src:string
    }[] | null
    source:string
}
export interface Message extends BaseMessage{
    time:number
    thread:string
    item:string
    status?:string
}
export interface ModifiedMessage extends Message {
    status:string
    type:string
}
