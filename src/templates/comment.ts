export interface CommentUser {
    id:number
    username:string
    avatar:string
}
export interface BaseComment {
    text:string
    post:string
    replyTo?:string
}
export interface Comment extends BaseComment {
    id:string
    time:number
    from:CommentUser
}