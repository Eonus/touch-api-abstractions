import { Message } from "../templates/message"

export interface ChatApiAccount {
    login:string
    defaultState?:boolean
    state?:boolean
    step:{ value:number, message:string } | null
    activated:boolean
}

export interface GetInfoResponse {
    clients:ChatApiAccount[]
    summary:{
        active:number,
        activated:number,
        demo:number,
        count:number,
        payment:{
            mode:string,
            balance:number
        }
    }
}

export interface ChatApi {
    addAccount(token:string, login:string, password:string, proxyString?:string, webhookUrl?:string):Promise<void>
    deleteAccount(token:string, login:string):Promise<void>
    setState(token:string, login:string, set:boolean):Promise<void>
    getInfo(token:string, login:string):Promise<ChatApiAccount>
    getInfoByToken(token:string):Promise<GetInfoResponse>
    sendMessage(token:string, message:Message):Promise<any>//TODO:add return type
}