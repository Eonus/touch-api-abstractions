import { ChatApi } from "./index";

export interface InstagramApi extends ChatApi{
    startChallenge(token:string, login:string):Promise<void>
    solveChallenge(token:string, login:string, code:string):Promise<void>
    twoFactorAuth(token:string, login:string, code:string):Promise<void>
}