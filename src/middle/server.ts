import { Middle } from ".";
import { Templatable } from "../api/templatable";

export interface ServerMiddle<SourceName> extends Middle<SourceName>, Templatable {
    
}