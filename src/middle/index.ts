export interface SourceAbstract <SourceName> {
    name:SourceName
    token:string
    middleOwner:string
}

export interface ChatApiAccountDataAbstract <SourceName> {
    login:string
    source:SourceAbstract <SourceName>
    accountHookValue:string
    middleOwner:string
}

export interface Middle <SourceName> {
    addAccount(middleOwner:string, source:SourceName, login:string, password:string):Promise<any>
    startAccount(middleOwner:string, source:SourceName, login:string):Promise<any>
    stopAccount(middleOwner:string, source:SourceName, login:string):Promise<any>
    getAccountsList(middleOwner:string):Promise<{accounts:ChatApiAccountDataAbstract<SourceName>[]}>
}